﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.DirectoryServices;
using System.DirectoryServices.ActiveDirectory;
using System.DirectoryServices.AccountManagement;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = ":: Login ::";

    }
    protected void btnReset_Click(object sender, EventArgs e)
    {

    }
    protected void btnLogin_Click(object sender, EventArgs e)
    {
        //Response.Redirect("~/Home.aspx", false);

        if (Auth_WithPric(txtUserName.Text, txtPassword.Text) == false)
        {
            lblError.Text = "Unable to login.";
        }
        else
        {
            Session["curUserName"] = txtUserName.Text;
            Session["LastEntry"] = "Initilized";

            try
            {
                Response.Redirect("~/Home.aspx", false);

            }
            catch (Exception ex)
            {
                Response.Write(ex.ToString());
            }


        }

        //lblError.Text = Auth_WithPric(txtUserName.Text, txtPassword.Text).ToString();

        //if(AuthUser(txtUserName.Text, txtPassword.Text) == "not found")
        //{
        //   lblError.Text = "Unable to login.";
        //}
        //else
        //{ 
        //    Response.Redirect("~/Home.aspx");

        //}

    }

    public bool Auth_WithPric(string user, string pass)
    {
        bool value; //, IsInAccessGroup = false ;
        //string result = "";
        using (var pc = new PrincipalContext(ContextType.Domain, "<FQDN>", "<FQDN in comman values.. forgot what it was called>"))
        {
            value = pc.ValidateCredentials(user, pass);

            if(value == false)
            {
                return value;
            }

            UserPrincipal userg = UserPrincipal.FindByIdentity(pc, user);

            if (userg != null)
            {
                // get the user's groups
                var groups = userg.GetAuthorizationGroups();

                foreach (GroupPrincipal group in groups)
                {
                    if(group.Name == "WebApp - VehicleBooking - Access")
                    {
                        return true;
                    }
                    // do whatever you need to do with those groups
                }
            }
        }
        return false;
    }

}
